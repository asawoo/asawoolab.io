The source for Asawoo documentation website: https://asawoo.gitlab.io

To run locally you need [hugo](https://gohugo.io/). Then run the server :

    hugo server

And open http://localhost:1313
