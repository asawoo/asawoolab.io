---
title: "Git Repositories"
menu: "main"
weight: 20
---

## Repositories

  * Avatar:
      * Implementation of the concept of avatar (a Web-compliant software
        extension of a thing).
      * <https://gitlab.com/asawoo/avatar>

  * Applications:
      * Applications and pieces of code that can be deployed on the ASAWoO WoT
        platforms.
      * <https://gitlab.com/asawoo/applications>

  * Platforms:
      * ASAWoO WoT platforms that can be deployed on a cloud infrastructure, on
        gateways or on physical objects.
      * <https://gitlab.com/asawoo/platform>

  * Devices:
      * Docker images and configuration files for ASAWoO-compliant devices.
      * <https://gitlab.com/asawoo/devices>

  * Semantic repositories:
      * The semantic repositories used in project ASAWoO to reason about
        application domains, adaptation tasks, code deployment, etc.
      * <https://gitlab.com/asawoo/semantic-repos>

  * Code repostory:
      * The code repository that contains the pieces of code that can be
        deployed on the ASAWoO platforms in order in order to control the
        physical things through their avatar representation.
      * <https://gitlab.com/asawoo/code-repo>



