---
title: "User's guide"
menu: "main"
weight: 40
---

An ASAWoO platform is composed of at least two parts: a WoT runtime and semantic
repositories. A third part (i.e. the code repository) can also be deployed if
needed (see section "Creation and execution of an ASAWoO platform" in the
document).

You can install the ASAWoO WoT runtime and the ASAWoO semantic repositories
either from a simple archive or using Docker images. These two procedures are
described below.

For more information about the ASAWoO platform, you must read the
[developers guide](developers_guide.md).

--------

## Installation of the WoT runtime and semantic repositories

`wget` and `bash` are needed in the following install procedure.  To install and
the WoT runtime and the semantic repositories without docker, you proceed as
follows:

    wget https://gitlab.com/asawoo/platform/-/jobs/artifacts/master/download?job=build-platform-archive
    unzip artifacts.zip
    cd asawoo
    ./start_semantic_repos &
    ./start_wot_runtime

--------

## Installation of the WoT runtime and semantic repositories using Docker

We provide Docker images to install the ASAWoO WoT runtime and the ASAWoO
semantic repositories on your machine.

Before starting, you must install `docker`, `docker-compose`, `curl` and `bash`.

### Docker installation

* On a Debian distribution:

      You must install Docker CE. To do so, you must follow the instruction given
      [here](https://docs.docker.com/engine/installation/linux/docker-ce/debian/).

      Then, you must install docker-compose

        apt-get update; apt-get install docker-compose

* On an Ubuntu distribution:

     You must install docker and docker-compose

      apt-get update; apt-get install docker docker-compose

### Deployment of the ASAWoO Docker images and creation of the Docker containers

Now, you must deploy on your machine the docker images we provide, and start the
docker containers created using these images.

    curl -s https://gitlab.com/asawoo/platform/raw/master/bin/install_wot_runtime.sh | bash -s

--------

## Configuration of the device using the WoT Administration Application

Connect to the following URL in order to manage your device using the WoT
administration application.

    http://localhost:9999/wotapps/admin/
