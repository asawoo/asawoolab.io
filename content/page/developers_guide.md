---
title: "Developer's guide"
menu: "main"
weight: 30
---

## Installation

It exists two kinds of platforms: platforms running in a cloud infrastructure,
and platforms running directly on the physical objects or on gateways they are
connected to. Platforms are based on OSGi/Felix (<http://felix.apache.org/>),
iPojo
(<http://felix.apache.org/documentation/subprojects/apache-felix-ipojo.html>)
and Vertx.io(<http://vertx.io/>).

An ASAWoO platform is composed of at least two parts: a WoT runtime and semantic
repositories. A third part (i.e. the code repository) can also be deployed if
needed (see section "Creation and execution of an ASAWoO platform" in the
document).

The ASAWoO platforms can probably be ported to other OSGi platforms such as
those listed below but no tests have been done so far.

 * [Apache Felix](http://felix.apache.org)
 * [Equinox](http://www.eclipse.org/equinox/)
 * [Knopflerfish](http://www.knopflerfish.org)
 * [Concierge](https://www.eclipse.org/concierge/)

To compile the source code of the ASAWoO platform you must have:

 * OpenJDK 8 or Oracle JDK 8
 * [Maven]([http://maven.apache.org])

If you want to build docker images, you must also install `docker` and
`docker-compose`.

To install docker on a Debian Linux machine, you must follow the installation
procedure described
[here](https://docs.docker.com/engine/installation/linux/docker-ce/debian/).

To install docker on a Ubuntu Linux machine, you must proceed as follows:

    apt update; apt install docker docker-compose

--------

## Get the source code from the git repositories

    # Clone of ASAWoO projects
    git clone https://gitlab.com/asawoo/avatar.git
    git clone https://gitlab.com/asawoo/applications.git
    git clone https://gitlab.com/asawoo/semantic-repos.git
    git clone https://gitlab.com/asawoo/platform.git
    git clone https://gitlab.com/asawoo/code-repo.git

--------

## Compilation

To compile the ASAWoO platform, you must proceed as follows:

    bin/install_standalone_mvn_deps.sh
    cd dependencies && mvn install ; cd -
    cd ../avatar && git pull && mvn install ; cd -
    cd ../applications && git pull && mvn install ; cd -
    cd ../code-repo && git pull && mvn install ; cd -
    cd ../semantic-repos && git pull && ./build_and_run.sh ; cd -

--------

## WoT Runtime Profiles
An ASAWoO WoT runtime can be configured with a profile. A profile defines the
bundles that must be installed before starting the runtime, or that must
deployed at startup according to a semantic description of the capacities of the
physical object. The bundles includes Java classes implementing capacities and
functionalities allowing to configure and to control physical objects.

Example of profiles are provided in directory `wot-runtime-profiles`.

Profiles can be defined hierarchically in order to not redefined a list of
bundles that has been already defined in another profile.  For example profile
`core-dtn.conf` extends profile `core.conf`. To perform such an extension, you
proceed as follows:

    . $ASAWOO_WOT_RUNTIME_PROFILES_DIR/parent_profile

    bundles_to_deploy="$bundles_to_deploy
    	${ASAWOO_CODE_REPO}/...
    "

where `$ASAWOO_WOT_RUNTIME_PROFILES_DIR` is the absolute path of directory
`wot-runtime-profiles`.

--------

## Creation and execution of an ASAWoO platform
An ASAWoO platform is composed of at least two parts: a WoT runtime and semantic
repositories. Instead of using the ASAWoO market place, an ASAWoO code
repository can also be deployed locally.

### Semantic repositories
To start a docker container hosting semantic repositories, you must execute the
following command:

    cd ../semantic-repos && git pull && ./build_and_run.sh ; cd -


### ASAWoO code repository
Three kinds of ASAWoO code repositories can be used: [the ASAWoO market
place](http://http://asawoo.liris.cnrs.fr/marketplace), a personnal code
repository accessible through an HTTP server, and your local maven repository
(i.e. `~/.m2/repository`).

By default, your local maven repository is used in the docker image of the ASAWoO
WoT runtime. To use the ASAWoO market place or a personnal repository accessible
through an HTTP server, you can set the environment variable ASAWOO_CODE_REPO to
[http://http://asawoo.liris.cnrs.fr/marketplace]
(http://http://asawoo.liris.cnrs.fr/marketplace) or to your own repository.

You can deploy your own code repository as follows:

    curl -s https://gitlab.com/asawoo/code-repo/raw/master/install.sh | bash -s

More details about the installation of a code repository can be found at:
[https://gitlab.com/asawoo/code-repo](https://gitlab.com/asawoo/code-repo).


### WoT Runtime
Then you can create a WoT runtime instance with a given profile, and start it as
follows :

    ./bin/dm wot-runtime-profiles/core.conf && ./bin/start.sh

Alternatively, you can run the ASAWoO WoT rutime in a docker image with a volume
to access your local maven code repository :

    docker build -t asawoo/runtime -f docker/Dockerfile .
    docker run --privileged -d --rm --name sensor1 -v ~/.m2/repository:/root/.m2/repository -p 8001:9999 -p 9001:9000 -e "ASAWOO_CODE_REPO=$ASAWOO_CODE_REPO" --network asawoo -ti asawoo/runtime

If you want to use your local maven repository as code repository, you must not
set the environment variable in the previous command line (i.e. you must remove
-e "ASAWOO_CODE_REPO=$ASAWOO_CODE_REPO" from the previous command line).

If you want to manage (i.e. to start, to stop, to delete) the WoT runtime and the semantic repositories using `docker-compose`, you can proceed as follows:

To start:

    docker-compose -f docker/docker-compose.platform.yml up -d || exit 1

To stop:

    docker-compose -f docker/docker-compose.platform.yml stop

Alternatively, you can used the script `./bin/asawoo_docker.sh`.

--------

## Creation of new Docker images
The script `./bin/asawoo_docker.sh` can help managing the ASAWoO platform using
docker.

To create a new Docker image of the ASAWoO runtime, you must proceed as follows:

    ./bin/asawoo_docker.sh build-runtime

This docker image is based on the `core` profile. If you want to build a docker
image with another profile, you must change the profile in file
`docker/Dockerfile`.

To destroy the docker image of the ASAWoO runtime, you can execute the following
script:

    ./bin/asawoo_docker.sh rm-runtime && ./bin/asawoo_docker.sh rm-runtime-image

--------

## Development of a new capability

A capacity implements a functionality. So, in the first place, if the implemented functionality does not exist, you have to define one.

### Domain and API
The functionality interface is a Java interface implementing asawoo.core.functionality.FunctionalityInterface provided by ASAWoO Avatar core module.
This interface must be annotated with the @Functionality annotation provided by asawoo.core.annotation package in ASAWoO Avatar core module.
Each interface method can be annotated with ASAWoO annotations (@Method to declare) and JAX-RS annotations (@Path and http methods @GET, @PUT, ...).

TODO EXAMPLE

### Implementation of a new capability
The capacity must implement the domain Functionality interface. It must also be published as an OSGi service (more conveniently by iPOJO annotations) in order to be discovered by the FunctionalityManager.

Bundle metadata stating which functionalities (interface) and capbilities (implementation) are provided by this bundle must be added. This can be done in the project POM file.
Developers should use asawoo.functionality and asawoo.capability directives.

TODO EXAMPLE

    <Provide-Capability>
      asawoo.functionality; name=AFunctionality,
      asawoo.capability; name=AFunctionalityImplementation; factory=asawoo.applications.things.afunctionality.AFunctionalityImplementation
    </Provide-Capability>

--------

## Development of a new functionality

TODO

--------

## Development of a new WoT application

The most important resource for WoT application development is the [Hello World application](https://gitlab.com/asawoo/applications/tree/master/wotapps/hello-world-wotapp).

It is a very simple application compatible with Asawoo. The client side code uses mostly the [vuejs framework](https://vuejs.org/) with the [vue-material UI components](https://vue-material-old.netlify.com/#/), it is built with [webpack](https://webpack.js.org/) that also provides a nice development server.

Even if your client side technical stack differs, the amount of code is minimal and it is probably a good idea to use this application as a skeleton and to start your project by copying it. Afterwards a simple string replace of all occurences of helloworld or hello-world in files and directory names should get you going. Read the README of the application for instructions on how to work from there.

This application is a Maven Java project with the right structure for compatibility with Asawoo. This compatibility is mostly ensured by these criterias:

  - A proper [*Provide-Capability* instruction](https://gitlab.com/asawoo/applications/blob/master/wotapps/hello-world-wotapp/pom.xml#L45) in the configuration of the [maven-bundle-plugin](http://felix.apache.org/documentation/subprojects/apache-felix-maven-bundle-plugin-bnd.html) for [felix](http://felix.apache.org/) in the pom.xml file.
  - One or many [*Require-Capability* instructions](https://gitlab.com/asawoo/applications/blob/master/wotapps/hello-world-wotapp/pom.xml#L42) to declare dependencies toward Asawoo functionalities
  - A [main Java class](https://gitlab.com/asawoo/applications/blob/master/wotapps/hello-world-wotapp/src/main/java/asawoo/wotapp/helloworld/HelloWorld.java) that implements the *asawoo.core.wotapp.WoTApp* interface and uses felix annotations to be instantiated at startup.
  - A build process that produces a directory *target/classes/WEB-INF* with an *index.html* file.

If your application is properly structured, built and available in the code repository, you should be able to run the Asawoo platform locally and see it among the other applications in the *Applications* section of the administration application.
