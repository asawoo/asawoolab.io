The "Web of Things" (WoT) extends the Internet of Things so that physical object
can be accessed and controlled using Web standards.

Project ASAWoO introduces the concept of "avatar" as a Web-compliant software
extension of a thing. Physical things can therefore be coupled with avatars, to
form cyber-physical objects that are compatible with WoT constraints and
infrastructures. Avatars achieve interoperability between things using semantic
technologies and expose high-level functionalities as RESTful resources. They
can collaborate with other avatars and provide WoT applications that match
end-users' needs. Avatars are implemented using a distributed software platform
that can be fully deployed on powerful objets, or distributed over
resource-constrained objects and a cloud infrastructure.


ASAWoO project is funded by the French National Research
Agency(<https://aap.agencerecherche.fr>) under contract ANR-13-INFR-0012-04.

Project documents (aka deliverables) are available on the [ASAWoO Web Site](https://liris.cnrs.fr/asawoo/doku.php?id=homepage)

--------
## Demonstration
This video shows the ASAWoO platform running on a ROS-based robot, on a base
station and on sensors. In this video, the robot is used to collect data from
sensors, as well as to perform a video surveillance. When the robot detects a
person in the area, it returns to its base station in order to deliver this
information.

<div align="center">

[![Alt text](https://img.youtube.com/vi/1WDgtVh7yRY/0.jpg)](https://www.youtube.com/watch?v=1WDgtVh7yRY)

</div>

--------
## ASAWoO Use Cases
In ASAWoO, we consider 3 use cases. These uses cases are depicted in the figure
below:

![ASAWoO use cases](/img/asawoo_use_cases.png)

### Standalone use case

In the standalone use case, we consider that the physical object has enough
memory, storage and computing capacities to run locally an ASAWoO platform
hosting its own avatar. As it is standalone, the avatar embeds all the
capacities, functionalities[^terminology] and WoT applications to control the
physical object.
If the physical object is expected to be self adaptive or/and to collaborate
with other things, it must host both the code repository and the semantic
repositories, otherwise these repositories should not be installed on the
physical object.

[^terminology]: Capacity, Functionality and WoT application are defined in the terminology section[](#terminology).

### Cloud use case

In the cloud use case, we consider three kinds of objects: 1)
resource-constrained objects, which have not enough resources to run an ASAWoO
platform, 2) objects that run only a part of their avatar due to their limited
resources and that require a cloud infrastructure to remotely run their
functionalities requiring a certain amount of computing and memory capacities,
3) and powerful objects that can run an ASAWoO platform (like in the standalone
use case).

In the cloud, we can deploy an ASAWoO platform to host the avatars of the
physical objects, a code repository and semantic repositories.

### Cloud and gateway use case

In the last case (i.e. the cloud and gateway use case), we consider that the
platform that hosts the avatars is installed on a domotic gateway and not in a
cloud. In this last case, only the code repository and the semantic repositories
are installed in the cloud. Like in the previous use case, an avatar can be
distributed on a physical object and on the gateway.

--------
## Terminology

* Avatar: A logical software extension of a physical thing. It hosts a set of
  functionalities, capacities and WoT applications that allow to configure and
  to control the physical object, or that allow the physical object to perform
  given tasks in an autonomous and/or collaborative way. It exposes some of its
  functionalities as REST services so that these functionalities can be
  exploited by other avatars or by pieces of code of WoT applications running on
  WoT clients.

* WoT Application: A Web application that makes it possible to configure and to
  control a physical object, or a set of physical objects. WoT applications are
  designed to be used by people. They are provided by an avatar, run
  simultaneously on the avatar side and on the WoT client side (i.e. on the Web
  browser of the user).

* Functionality: A functionality is a piece of code that is semantically
  described, that performs a computing, sensing or action task, and that is
  independent on a specific object.

* Capacity: A capacity is a low-level functionality, whose implementation
  depends on a physical object. A capacity is also semantically described. It
  can be viewed as a "glue" between a functionality --that is independent on an
  object-- and a physical object.

 * WoT Runtime: The runtime that hosts and execute the avatars.

 * WoT Platform: The WoT ASAWoO platform is composed of a (or a set of) WoT
   runtime(s), of semantic repositories and of a code repository.

 * Code repository: A repository from which avatars can download functionalities
   to install them locally.

--------
## Avatar and platform architecture

### Avatar architecture

As shown in the figure below, an avatar is based on a set of managers.
![Avatar architecture](/img/archi_avatar.png)


### Platform architecture
A platform can host serveral avatars. A platform is basically composed of a
runtime environment hosting avatars, of a set of semantic repositories (context,
capacities, functionalities, and WoT application), and of a code repository. A
platform also has a manager to instanciate the avatars.

![Platform architecture](/img/Infra_WoT.png)

